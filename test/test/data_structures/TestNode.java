package test.data_structures;

import static org.junit.Assert.assertTrue;

import model.data_structures.*;

public class TestNode {

	/**
	 * Construye un nuevo nodo de la lista.
	 * @param pSiguiente Es el nuevo nodo al que se le va a conectar en la lista.
	 */
	private Node nodo;
	private Node siguiente;
	private Node anterior;
	
    public void setup1()
    {
    	nodo= new Node(1);
    	siguiente= new Node (2);
    	anterior= new Node (0);
    	
    }
    
    public void setup2()
    {
    	nodo= new Node("a");
    	siguiente= new Node ("b");
    	anterior= new Node ("z");
 	
    }
    
    public void cambiarSiguiente( Node pSiguiente)
    {
    	setup1();
    	nodo.cambiarSiguiente(siguiente);
    	assertTrue(nodo.darSiguiente()!=null);
    }
    
    public void cambiarAnterior( Node pSiguiente)
    {
    	setup1();
    	nodo.cambiarAnterior(anterior);
    	assertTrue(nodo.darAnterior()!=null);
    }

}
