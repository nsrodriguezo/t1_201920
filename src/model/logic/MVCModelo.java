package model.logic;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.opencsv.CSVReader;

import model.data_structures.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	
	
	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico datos;
	
	private ListaEncadenada<Viaje> viajes;

	private CSVReader pars;
	
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico(7);
	}
	
	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int capacidad)
	{
		datos = new ArregloDinamico(capacidad);
	}
	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(String dato)
	{	
		datos.agregar(dato);
	}
	
	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(String dato)
	{
		return datos.buscar(dato);
	}
	
	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(String dato)
	{
		return datos.eliminar(dato);
	}
	
	
	public void loadStations (String UberFile) {

	   viajes = new ListaEncadenada<Viaje>();
		try
		{
			Reader lector = Files.newBufferedReader(Paths.get(UberFile));

			pars = new CSVReader(lector);

			//desprecio la linea de tipo
			String[] linea = pars.readNext();

			while ((linea = pars.readNext()) != null) 
			{
				Viaje actual =new Viaje(linea[0],linea[1],linea[2],linea[3],linea[4],linea[5],linea[6]);
				System.out.println("su madre :"+ linea[6]);
				viajes.add(actual);
			}
		}
		catch (Exception e) 
		{
			System.out.println("archivo corrupto :c");
		}		
	}

	
}
