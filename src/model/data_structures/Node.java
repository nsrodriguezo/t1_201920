package model.data_structures;

public class Node <T> {
	
	/**
	 * Representa el elemento del nodo.
	 */	
	
	private T elemento;
	
	/**
	 * Representa la referencia al siguiente elemento.
	 */
	
	private Node<T> siguiente;
	
	/**
	 * Representa la referencia al anterior elemento.
	 */
	
	private Node<T> anterior;
	
	/**
	 * Construye un nuevo nodo de la lista.
	 * @param pSiguiente Es el nuevo nodo al que se le va a conectar en la lista.
	 */
	
	public Node (T pElemento)
	{
		elemento = pElemento;
		siguiente = null;
		anterior = null;
	}
	
	/**
	 * Permite cambiar la referencia del siguiente elemento por otro.
	 * @param pSiguiente Es un nodo al que le va a apuntar el nodo actual.
	 */
	
	public void cambiarSiguiente (Node<T> pSiguiente)
	{
		siguiente = pSiguiente;
	}
	
	/**
	 * Permite cambiar la referencia del anterior elemento por otro.
	 * @param pSiguiente Es un nodo al que le va a apuntar el nodo actual.
	 */
	
	public void cambiarAnterior (Node<T> pAnterior)
	{
		anterior = pAnterior;
	}
	
	/**
	 * Permite retornar el siguiente nodo enlazado con respecto a mi.
	 * @return Mi siguiente nodo.
	 */
	
	public Node<T> darSiguiente()
	{
		return siguiente;
	}
	
	/**
	 * Permite retornar el anterior nodo enlazado con respecto a mi.
	 * @return Mi anterior nodo.
	 */
	
	public Node<T> darAnterior()
	{
		return anterior;
	}
	
	/**
	 * Permite devolver el elemento contenido en el nodo
	 * @return Elemento contenido
	 */
	
	public T darElemento()
	{
		return elemento;
	}
	
	/**
	 * Permite desconectar el siguiente elemento y apuntar al siguiente del siguiente
	 */
	
	public void desconectarSiguiente()
	{
		siguiente = siguiente.siguiente;
	}
	
	/**
	 * Permite desconectar el anterior elemento y apuntar al anterior del anterior.
	 */
	
	public void desconectarAnterior()
	{
		anterior = anterior.anterior;
	}
}
