package model.data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Steven Rodriguez
 * Invariante:
 * 
 * longitud >= 0
 * primerElemento != null => primerElemento.darAnterior() == null
 * ultimoElemento != null => ultimoElemento.darSiguiente() == null 
 */

public class ListaEncadenada <T extends Comparable<T>> implements LinkedList<T>, Iterable<T>{

	/**
	 * Referencia al nodo del primer elemento de la lista encadenada.
	 */

	private Node<T> primerElemento;

	/**
	 * Referencia al nodo del ultimo elemento de la lista.
	 */

	private Node <T> ultimoElemento;

	/**
	 * Referencia al nodo del elemento actual de la lista.
	 */

	private Node <T> elementoActual;

	/**
	 * Cantidad de nodos con elementos en la lista encadenada
	 */

	private int longitud;

	/**
	 * Permite crear una lista encadenada
	 */

	public ListaEncadenada ()
	{
		primerElemento = null;
		elementoActual = primerElemento;
		ultimoElemento = null;		
		longitud = 0;
	}	

	/**
	 * Permite agregar un nuevo elemento a la lista encadenada.
	 * @precondicion El elemento a agregar NO puede ser nulo.
	 * @param elemento: Es el nuevo elemento que se desea agregar a la nueva lista.
	 */

	public void add(T elemento) {

		// Caso #1 Lista Vacia.

		Node <T> nodoNuevo = new Node<T>(elemento);

		if (primerElemento == null)
		{
			primerElemento = nodoNuevo;
			ultimoElemento = nodoNuevo;
			elementoActual = primerElemento;
		}

		// Caso #2 Por lo menos hay un elemento. El nuevo elemento es menor en comparacion.

		else if (elemento.compareTo(primerElemento.darElemento()) <= 0)
		{
			nodoNuevo.cambiarSiguiente(primerElemento);
			primerElemento.cambiarAnterior(nodoNuevo);
			if (primerElemento.darSiguiente() == null) ultimoElemento = primerElemento;
			primerElemento = nodoNuevo;	
			elementoActual = primerElemento;
		}

		// Caso #3 Por lo menos hay un elemento y el nuevo es mayor en comparacion

		else if (elemento.compareTo(primerElemento.darElemento()) > 0)
		{
			Node <T> punteroAnterior = primerElemento;
			Node <T> punteroActual = primerElemento.darSiguiente();

			while (punteroActual != null && elemento.compareTo(punteroActual.darElemento()) >= 0)
			{
				punteroAnterior = punteroActual;
				punteroActual = punteroActual.darSiguiente();
			}			

			if (punteroAnterior.darSiguiente() == null) // Estoy parado en el ultimo nodo de la lista
			{
				punteroAnterior.cambiarSiguiente(nodoNuevo);
				nodoNuevo.cambiarAnterior(punteroAnterior);				
				ultimoElemento = nodoNuevo;
			}

			else // Estoy parado en un nodo intermedio
			{
				nodoNuevo.cambiarSiguiente(punteroAnterior.darSiguiente());
				nodoNuevo.cambiarAnterior(punteroAnterior);
				punteroAnterior.cambiarSiguiente(nodoNuevo);
				nodoNuevo.darSiguiente().cambiarAnterior(nodoNuevo);
			}					
		}

		longitud++;
		verificarInvariante();		
	}

	/**
	 * Permite agregar un elemento al inicio de la lista encadenada
	 * @param elemento Es el elemento a ser agregado al inicio de la lista.
	 */
	
	public void addFirst(T elemento) {
		
		Node <T> nodoNuevo = new Node<T>(elemento);
		
		if (primerElemento == null)
		{
			primerElemento = nodoNuevo;
			ultimoElemento = nodoNuevo;
		}
		
		else
		{
			nodoNuevo.cambiarSiguiente(primerElemento);
			primerElemento.cambiarAnterior(nodoNuevo);
			primerElemento = nodoNuevo;
		}
		
		longitud++;
		verificarInvariante();
	}
	
	/**
	 * Permite eliminar un elemento de la lista pasado por parametro
	 * @param elemento. Es el elemento que se desea buscar sobre la lista encadenada
	 */

	public void delete(T elemento) {

		// Caso #1 Lista vacia

		if (primerElemento == null)
		{
			return;			
		}

		// Caso #2 Se debe eliminar el primer elemento.

		else if (primerElemento.darElemento().equals(elemento))
		{
			Node <T> auxiliarBorrado = primerElemento;
			primerElemento = primerElemento.darSiguiente();			
			auxiliarBorrado.cambiarSiguiente(null);

			if (primerElemento != null)
			{
				primerElemento.cambiarAnterior(null);
			}

			elementoActual = primerElemento;
		}

		// Caso #3 Se debe buscar el elemento para eliminarlo 

		else
		{
			Node <T> auxiliarBorrado = get(elemento);

			if (auxiliarBorrado == null) // El elemento se busco pero no se encontro
			{
				return;
			}

			else if(auxiliarBorrado.darSiguiente() == null) // Ultimo Elemento
			{
				auxiliarBorrado.darAnterior().desconectarSiguiente();
				auxiliarBorrado.desconectarAnterior();
			}

			else // Caso General: Borrar elemento intermedio.
			{
				auxiliarBorrado.darAnterior().desconectarSiguiente();
				auxiliarBorrado.darSiguiente().desconectarAnterior();
				auxiliarBorrado.cambiarSiguiente(null);
				auxiliarBorrado.cambiarAnterior(null);
			}			
		}

		longitud--;
		verificarInvariante();
	}

	/**
	 * Permite obtener un nodo de la lista que contiene cierto elemento que se desee buscar
	 * @param elemento: Es el elemento que se desea buscar sobre la lista encadenada.
	 * @return El nodo del elemento si existe. En caso contrario, el metodo retorna NULL. 
	 */

	public Node<T> get(T elemento) {

		Node <T> actual = primerElemento;

		while (actual != null)
		{
			if (actual.darElemento().equals(elemento))
			{
				return actual;
			}

			actual = actual.darSiguiente();
		}

		return null;
	}	

	@Override
	public int size() {

		return longitud;

	}
	
	
	/**
	 * Permite llegar a un elemento de la lista por un indice dado
	 * @param posicion Es el numero del indice al cual se desea accesar.
	 * @return El nodo que ocupa la posicion deseada. NULL si la posicion no existe en la lista.
	 */
	public Node<T> getPosition(int posicion) {

		Node <T> punteroActual = primerElemento;

		if (posicion < 0 || posicion > longitud -1)
		{
			return null; // Indice invalido
		}

		int pasosDados = 0;

		while (punteroActual != null && pasosDados < posicion)
		{
			punteroActual = punteroActual.darSiguiente();
			pasosDados++;
		}

		return punteroActual;
	}

	/**
	 * Permite devolver el elemento actual de la lista. 	
	 */

	public Node<T> getCurrent() {

		return elementoActual;
	}

	/**
	 * Permite avanzar un elemento en la lista.
	 */

	public void next() {		

		if (elementoActual != null && elementoActual.darSiguiente() != null)
		{
			elementoActual = elementoActual.darSiguiente();
		}		
	}
	
	public boolean estaVacio()
	{
		return primerElemento == null;
	}

	@Override
	public Iterator<T> iterator() {		
		return new Iterador<T>(primerElemento);		
	}

	private void verificarInvariante ()
	{		
		if(primerElemento != null)
		{
			assert primerElemento.darAnterior() == null : "[ListaEncadenada] El primer elemento presenta elementos detras de l";
		}

		if (ultimoElemento != null)
		{
			assert ultimoElemento.darSiguiente() == null: "[ListaEncadenada] El ultimo elemento presenta elemento despues de l";
		}

		assert longitud >= 0: "El valor de la longitud es invalido";
	}
	
	/**
	 * Permite retornar el primer elemento en la lista.
	 * @return Primer elemento de la lista
	 */
	
	public T darPrimerElemento()
	{
		return primerElemento.darElemento();
	}
	
	/**
	 * Permite retornar el ultimo elemento en la lista.
	 * @return Ultimo elemento de la lista
	 */
	
	public T darUltimoElemento()
	{		
		return ultimoElemento.darElemento();
	}

	@SuppressWarnings("hiding")
	public class Iterador<T extends Comparable<T>> implements Iterator<T>
	{
		/**
		 * Puntero del iterador para avanzar en la lista encadenada.
		 */

		private Node <T> punteroAvance;

		/**
		 * Permite crear un iterador sobre la lista encadenada.
		 * @param punteroInicial Debe ser el primer elemento de la lista.
		 */

		public Iterador(Node <T> punteroInicial) 
		{			
			punteroAvance = punteroInicial;			
		}

		@Override		
		public boolean hasNext() {			
			return (punteroAvance != null) ? true : false;
		}

		@Override
		public T next() {
			
			if (punteroAvance == null)
				throw new NoSuchElementException("No hay proximo elemento por visitar");
			
			T elemento = punteroAvance.darElemento();
			punteroAvance = punteroAvance.darSiguiente();			
			return elemento;
		}	
	}					
}
